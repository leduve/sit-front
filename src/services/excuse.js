import axios from 'axios'
import QueryString from 'qs'
import store from '../store'

async function retrieveExcuses (hostname) {
  const locationData = 'http://' + hostname + ':8000/excuse/all/'
  const res = await axios.get(locationData)
  return await res.data
}

function addExcuse (hostname, tag, excuse) {
  const locationData = 'http://' + hostname + ':8000/excuse/add/'
  return axios(locationData, {
    method: 'post',
    data: QueryString.stringify({ tag: tag, excuse: excuse }),
    config: {
      headers: {
        'Access-Control-Allow-Credentials': true
      }
    }
  }).then((response) => {
    return response.data
  })
}

function giveMeExcuse (oldOneId) {
  const excuses = store.getters.excuses
  const index = excuses.indexOf(
    excuses.find((excuse) => excuse.pk === oldOneId)
  )
  return index === excuses.length - 1 ? excuses.at(0) : excuses.at(index + 1)
}

function giveMeExcuseByHttp (httpCode) {
  return store.getters.excuses.find((excuse) => excuse.fields.http_code === httpCode)
}

function giveMeRandomInt (max) {
  return Math.floor(Math.random() * max)
}

export {
  retrieveExcuses,
  giveMeExcuse,
  giveMeExcuseByHttp,
  giveMeRandomInt,
  addExcuse
}
