import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    excuses: []
  },
  mutations: {
    setExcuses (state, value) {
      state.excuses = value
    }
  },
  getters: {
    excuses (state) {
      return state.excuses
    }
  },
  actions: {},
  modules: {}
})
